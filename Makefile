TOPTARGETS := all
SUBDIRS := TwoPad SplineFit PhysMore SLitrani VisuSLitrani

all: lib subdirs cp_pcm 

.PHONY: subdirs $(SUBDIRS)

clean:
	@rm -vf include/*
	@rm -vf */*.o
lib:
	@mkdir -p bin
	@mkdir -p lib
	@mkdir -p include

subdirs: $(SUBDIRS)
$(SUBDIRS):
		$(MAKE) -C $@ clean; \
		$(MAKE) -C $@; \
		$(MAKE) -C $@ install;

SplineFit: TwoPad
PhysMore: SplineFit
SLitrani: PhysMore
VisuLitrani: SLitrani
cp_pcm:
	@rm -vf SLitraniDict_rdict.pcm
	@ln -s SLitrani/SLitraniDict_rdict.pcm .
	@rm -vf bin/SLitraniDict_rdict.pcm
	@ln -s ../SLitrani/SLitraniDict_rdict.pcm bin/.
