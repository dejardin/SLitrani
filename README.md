# SLitrani
Make SLitrani version of F.X. Gentit 

https://crystalclear.web.cern.ch/crystalclear/SLitraniX/SLitrani/index.html

works with recent versions of gcc and root.

Customized to add possibilities to generate photons from the output of Geant4 to :
- Simulate accurate shower shape in CMS-ECAL crystals
- Simulate Cerenkov photons in crystals

Also
- Emmission spectrum from CMS-Note 2009-016
- Clean all macros in FitMacros in order to be able to update databases with new spectra.
- New SPlineFit RadDamage_vs_z_CMS_PbWO4 (no more used, see bellow)
- Add new methods to transfer the steering of radiation damages to user space : TLitVolume::SetUserDamage(Bool_t, TF1 * , dir)
- Start to use eta-Z dependency of radiation damages as used in CMS-ECAL-WG3
- Find a way to SLitrani to look for spectra DB where their should be and not only where the program is running
- Put DC3145RTV glue absorption length
- Put APD Epoxy window absorption length 
- Put APD QE
- Add a photon gun in front of the APD to check basic perforamces of SLitrani when changing mu_induced

Have a loot at G4Geom.C macro in SMacros directory<br>
- Duplicate geometry from Geant4 simulation. Done for testbeam on 25-crystal matrix and full SM.
- One face is depolished
- Porte-ferule with fiber to inject monitoring light
- Capsule with two APDs and glue
- Aluminized alveola to reflect photons
- No chamfers for the moment on EB crystals (do we care ?)
- EE case not yet implemented, since it need VPT description and chamfers on crystals which should be depolished
- TF2 function to interface dose and fluence maps from WG3 and SLitrani attenuation computation.
- Implement WG3 tools to handle lumi correctly. Thanks to Sasha Ledovskoy for all the explanations 
- Make numerical integration to define default density vs z for the requested luminosity according to recorded lumi profile

For convenience, be sure that you have a ~/.rootrc defined and contains

Unix.*.Root.DynamicPath:    .:/usr/lib64/root:/data/cms/SLitrani/lib

Unix.*.Root.MacroPath:      .:/usr/share/root/macros:/data/cms/SLitrani/SMacros:/data/cms/SLitrani/FitMacros

Tricks : <br>
If you encounter a segfault or coredump when you try to run a macro in SLitrani, be sure you instantiated TLit.
Have a look at SMacros/InitSLitrani.C. It is not obvious, but TLit::SetParmeters() does the job.

To install SLitrani:<br>
- Clone this cite
- source setup.sh (See if the ggc/root version are correct for you)
- make (Look at Makefile for details)
<br>

Creation : Marc Dejardin - 2018/03/12 <br>
Last mod : Marc Dejardin - 2024/11/15
