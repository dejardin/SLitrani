#include <stdio.h> 
#include <stdlib.h> 
#include "TGraph.h" 
void make_mu_induced_EM()
{
  FILE *fd, *fd1, *fd2, *fd3;
  fd= fopen("mu_induced_EM.txt","r");
  fd1=fopen("mu_induced_EM_1.txt","r");
  fd2=fopen("mu_induced_EM_2.txt","r");
  fd3=fopen("mu_induced_EM_3.txt","r");
  Double_t x[30],y[30],x1[30],y1[30],x2[30],y2[30],x3[30],y3[30];

  Int_t eof=0;
  Int_t n=0;
  while(eof!=EOF)
  {
    eof=fscanf(fd,"%lf %lf",&x[n],&y[n]);
    if(eof==EOF)continue;
    n++;
  }
  fclose(fd);

  eof=0;
  Int_t n1=0;
  while(eof!=EOF)
  {
    eof=fscanf(fd1,"%lf %lf",&x1[n1],&y1[n1]);
    if(eof==EOF)continue;
    n1++;
  }
  fclose(fd1);

  eof=0;
  Int_t n2=0;
  while(eof!=EOF)
  {
    eof=fscanf(fd2,"%lf %lf",&x2[n2],&y2[n2]);
    if(eof==EOF)continue;
    n2++;
  }
  fclose(fd2);

  eof=0;
  Int_t n3=0;
  while(eof!=EOF)
  {
    eof=fscanf(fd3,"%lf %lf",&x3[n3],&y3[n3]);
    if(eof==EOF)continue;
    n3++;
  }
  fclose(fd3);
  TGraph *tg=new TGraph(n,x,y);
  tg->SetLineWidth(2);
  tg->SetLineColor(kBlack);
  TGraph *tg1=new TGraph(n1,x1,y1);
  tg1->SetLineWidth(2);
  tg1->SetLineColor(kBlue);
  TGraph *tg2=new TGraph(n2,x2,y2);
  tg2->SetLineWidth(2);
  tg2->SetLineColor(kGreen);
  TGraph *tg3=new TGraph(n3,x3,y3);
  tg3->SetLineWidth(2);
  tg3->SetLineColor(kOrange);
  tg->Draw("alp");
  gPad->SetGridx();
  gPad->SetGridy();
  tg1->Draw("lp");
  tg2->Draw("lp");
  tg3->Draw("lp");
}
